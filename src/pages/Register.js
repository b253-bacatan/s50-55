import { useState, useEffect, useContext } from 'react';

import { Form, Button } from 'react-bootstrap';

import { Navigate } from 'react-router-dom';

import UserContext from '../UserContext';

import Swal from 'sweetalert2';



export default function Register(){

	const { user } = useContext(UserContext);

	// State hooks to store the values of the input fields
	// getters are variables that store data (from setters)
	//setters are function that sets the data (from getter)
	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');

	const [mobileNumber, setMobileNumber] = useState('');
	const [firstName, setFirstName] = useState ('');
	const [lastName, setLastName] = useState ('');


	// State to determine whether submit button is enabled or not
	const [isActive, setIsActive] = useState(false);

	const [ registerSuccess, setRegisterSuccess ] = useState(false);

	console.log(email);
	console.log(password1);
	console.log(password2);

	// Function to simulate user registration
	function registerUser(e) {
		// Prevents page redirection via form submission
		e.preventDefault();


		fetch('http://localhost:4000/users/checkEmail',
		{
			method: "POST",
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email
			}),

		})
		.then((res) => res.json())
		.then((data) => {

			if(data) {
				Swal.fire({
					title: "Authentication Failed",
					icon: "error",
					text: "Email already exists"
				});
				
			} else {

				fetch('http://localhost:4000/users/register', {

					method: "POST",
					headers: {
						"Content-Type": "application/json",

					},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						email: email,
						mobileNumber: mobileNumber,
						password: password1
					})

			}).then((res) => res.json())
			  .then((data) => {

			  	if(data) {
			  
			   Swal.fire({
					title: "Login Successful",
					icon: "success",
					text: "Welcome to Zuitt!"
		
				});
			  	setRegisterSuccess(true)
			  
			  } else {

				Swal.fire({
					title: "Registration failed",
					icon: "error",
					text: "please make sure you have registered."
				});
			}
		}).catch((error) => console.log(error))
	} 
 })
		


		// setEmail('');
		// setPassword1('');
		// setPassword2('');
		// setFirstName("");
		// setLastName("");
		// setMobileNumber("");

		// alert("Thank you for registering")

	}


	useEffect(() => {



		// Validation to enable submit button when all fields are populated and both passwords match
		
		if((firstName !== '' && firstName.length >= 11 && lastName !== '' && lastName.length >= 11 && email !== '' && password1 !== '' && password2 !== '' && mobileNumber.length >= 11) && (password1 === password2)) {

			setIsActive(true);
		} else {

			setIsActive(false);
		}

	}, [email, password1, password2, firstName, lastName, mobileNumber]);


	return registerSuccess !== false ? (
		<Navigate to="/login"/>
		)
			:
		(
		// 2-way Binding (Binding the user input states into their corresponding input fields via the onchange JSX event handler)
		<Form onSubmit={(e) => registerUser(e)}>
	          <Form.Group className="mb-3" controlId="userFirstName">
		  	        <Form.Label>First Name</Form.Label>
		  	        <Form.Control
		  	         type="firstName" 
		  	         placeholder="Enter first name"
		  	         value={ firstName }
		  	         onChange={e => setFirstName(e.target.value)}
		  	         required />
	          </Form.Group>
	                <Form.Group className="mb-3" controlId="userLastName">
	          	        <Form.Label>Last Name</Form.Label>
	          	        <Form.Control
	          	         type="lastName" 
	          	         placeholder="Enter last name"
	          	         value={ lastName }
	          	         onChange={e => setLastName(e.target.value)}
	          	         required />
	                </Form.Group>
		      <Form.Group className="mb-3" controlId="userEmail">
			        <Form.Label>Email email</Form.Label>
			        <Form.Control
			         type="email" 
			         placeholder="Enter email"
			         value={ email }
			         onChange={e => setEmail(e.target.value)}
			         required />
			        <Form.Text className="text-muted">
			          We'll never share your email with anyone else.
			        </Form.Text>
		      </Form.Group>
		            <Form.Group className="mb-3" controlId="mobileNumber">
		      	        <Form.Label>Mobile Number</Form.Label>
		      	        <Form.Control
		      	         type="string" 
		      	         placeholder="Mobile Number"
		      	         value={ mobileNumber }
		      	         onChange={e => setMobileNumber(e.target.value)}
		      	         required />
		            </Form.Group>

		      <Form.Group className="mb-3" controlId="password1">
			        <Form.Label>Password</Form.Label>
			        <Form.Control 
			        type="password" 
			        placeholder="Password"
			        value={ password1 }
			        onChange={e => setPassword1(e.target.value)}
			        required />
			   </Form.Group>

		       <Form.Group className="mb-3" controlId="password2">
		   	        <Form.Label>Verify Password</Form.Label>
		   	        <Form.Control 
		   	        type="password" 
		   	        placeholder="Verify Password"
		   	        value={ password2 }
			        onChange={e => setPassword2(e.target.value)}
		   	        required />
		   	   </Form.Group>

		   { isActive ? 

			   <Button variant="primary" type="submit" id="submitBtn">
				  Submit
			   </Button>
			   :
			   <Button variant="danger" type="submit" id="submitBtn" disabled>
			     Submit
			   </Button>
		   }   
		</Form>

	)
}





// The JWT will be used to retrieve user information across the the whole frontend application and storing it in the localStorage will allow ease of access to the user's information

// Implement sweetalert2 for login page