import { Navigate } from 'react-router-dom';

import UserContext from '../UserContext';

import { useContext, useEffect } from 'react';

export default function Logout() {

	// Consume the UserContext object and destructure it to access the user state and unsetUser function from the context provider
	const { unsetUser, setUser } = useContext(UserContext);

	unsetUser();
	
	// localStorage.clear(); - already implemented in App.js

	// Redirect back to Login

	useEffect(() => {

		setUser({id: null})
	});

	
	return(

		<Navigate to='/login'/>
	)
}